
obj-m = keyboard_x.o

# obj-m := modu1.o modu2.o modu4.o modu5.o modu6.o modu7b.o

# EXTRA_CFLAGS += -w  

KDIR := /lib/modules/$(shell uname -r)/build
PWD  := $(shell pwd)

all:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
#	make -C $(KDIR) M=$(PWD) modules

clean:
	@rm -rf *~ *.o *.mod.c .*.cmd .tmp_versions *.ko *.mod.o *.mod