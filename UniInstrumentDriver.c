#include <linux/slab.h>      
#include <linux/fs.h>           
#include <linux/errno.h>       
#include <linux/types.h>               
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>
#include <linux/types.h>
#include <linux/uaccess.h>

//#include <linux/lock

static int   debug = 0;
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))


/**
 * Spezifische Information für ein Device 
 */
static struct usb_device *device;
static struct usb_class_driver class;
const int  USB_NI_MINOR_BASE = 1;
static int  major = 240;

/*
 * Die Benötigte information für eine Verbindung mt dem Device 
 */

 unsigned char *          bulk_in_buffer_1; 
 unsigned char *          bulk_in_buffer_2; 
    size_t                bulk_in_size = 8;   
    __u8                  bulk_in_add_1;    
    __u8                  bulk_out_add_1;
    __u8                  bulk_in_add_2;    
    __u8                  bulk_out_add_2;


/*
 * Falls Debuging nötig, debug signale werden als parameter empfangen
 */
module_param(debug, int, 0644);


// Einstellungen für die Adresse für Input und Output bzw. ihr Buffer
// In diesem Gerät gibt es zwei Input und zwei Output dafür braucht man zwei Buffer 
static void set_bulk_address (struct usb_interface *interface) {

    struct usb_endpoint_descriptor *endpoint;
    struct usb_host_interface *iface_desc;
    int i;

    iface_desc = interface->cur_altsetting;

    for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
        endpoint = &iface_desc->endpoint[i].desc;
        printk(KERN_INFO "Endpoints are (%d)", endpoint);

        //printk(KERN_INFO "Endpoints attriputes are (%d)", endpoint->bmAttributes);
        //printk(KERN_INFO "Endpoints Mask are (%d)", USB_ENDPOINT_XFERTYPE_MASK);
        //printk(KERN_INFO "Endpoints Bulk are (%d)", USB_ENDPOINT_XFER_BULK);
        //printk(KERN_INFO "Endpoints USB_DIR_IN (%d)", USB_DIR_IN);
        //printk(KERN_INFO "Endpoints Address --->  (%d)", endpoint->bEndpointAddress);
        //printk(KERN_INFO "Endpoints Max Package Size--->  (%d)", endpoint->wMaxPacketSize);

     
        /* check for bulk endpoint */
        if ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) 
            == USB_ENDPOINT_XFER_BULK){

            /* bulk in */
            if(endpoint->bEndpointAddress & USB_DIR_IN) {

                //printk(KERN_INFO "has input -(%d)-------------------", i);
                //printk(KERN_INFO " Its Size -(%d)-------------------", bulk_in_size);
                if(i == 0){
                    // Für das Erste input
                    bulk_in_add_1 = endpoint->bEndpointAddress;
                    bulk_in_size = endpoint->wMaxPacketSize;
                    bulk_in_buffer_1 = kmalloc(bulk_in_size,GFP_KERNEL);
                    
                    if(!bulk_in_buffer_1)
                       printk(KERN_INFO "Could not allocate bulk buffer 1");

                    else
                       printk(KERN_INFO "Allocated buffer 1 with (%d) ----> ", sizeof(bulk_in_buffer_1));
                     
                
                    }
                else{
                    // Für das Zweite input
                    bulk_in_add_2 = endpoint->bEndpointAddress;
                    bulk_in_size = endpoint->wMaxPacketSize;
                    bulk_in_buffer_2 = kmalloc(bulk_in_size,GFP_KERNEL);
                    
                    if(!bulk_in_buffer_2)
                        printk(KERN_INFO "Could not allocate bulk buffer 2");
                    
                    else
                        printk(KERN_INFO "Allocated buffer 2 with (%d) ----> ", sizeof(bulk_in_buffer_2));
                    
                }
            }
            /* bulk out */
            else
               { 
                if(i == 1){
                    bulk_out_add_1 = endpoint->bEndpointAddress;
                    printk(KERN_INFO " has output -(%d)-------------------", i);
                    printk(KERN_INFO " Its Adress -(%d)-------------------", bulk_out_add_1);
                }else{
                    bulk_out_add_2 = endpoint->bEndpointAddress; 
                    printk(KERN_INFO " has output -(%d)-------------------", i);
                    printk(KERN_INFO " Its Adress -(%d)-------------------", bulk_out_add_2);
                }
               } 
        }
    }
}
/*
int print_x(char *str)
{
  struct tty_struct *dev_tty;

  dev_tty = get_current_tty();
  if (dev_tty==NULL) return -ENOTTY;

  (dev_tty->ops->write)
         (dev_tty, str, strlen(str));

  return 0;
}
*/
static ssize_t device_write(struct file *filp, 
                      const char *buffer, size_t len, loff_t *offs)
{
  unsigned int i=0;

  while(i<len){
      if(i == bulk_in_size)
        break;

      if(get_user(bulk_in_buffer_1[i], buffer++)){
          return -EFAULT;
      }

      printk(KERN_ERR "Resived in the bulk -->  %d\n", bulk_in_buffer_1[i]);
      i++; 
    }
     
    int retval;
    int read_cnt;
    
    /* schreibe zum output*/
    retval = usb_bulk_msg(device, usb_sndbulkpipe(device, bulk_out_add_1),
            bulk_in_buffer_1, bulk_in_size, &read_cnt, 5000);
    if (retval)
    {
        printk(KERN_ERR "The Send Bulk message returned %d\n", retval);
        return retval;
    }


    //return MIN(cnt, read_cnt);
    
  printk(KERN_INFO "Die Ausgabe wurde gesendet %d \n", bulk_in_buffer_1[1]);
  if (debug>2) 
  //print_x(me);

  return i;
}

static int device_open(struct inode *i, struct file *f)
{
    return 0;
}
static int device_release(struct inode *i, struct file *f)
{
    return 0;
}

static ssize_t device_read(struct file *f, char __user *buf, size_t cnt, loff_t *off)
{
    int retval;
    int read_cnt;
    
    /* Lese vom ersten Input */
    retval = usb_bulk_msg(device, usb_rcvbulkpipe(device, bulk_in_add_1),
            bulk_in_buffer_1, bulk_in_size, &read_cnt, 10000);
    if (retval)
    {
        printk(KERN_ERR "The Read Bulk message returned %d\n", retval);
        return retval;
    }
    if (copy_to_user(buf, bulk_in_buffer_1, MIN(cnt, read_cnt)))
    {
        return -EFAULT;
    }
          printk(KERN_INFO "Die Eingabe wurde empfangen %d \n", bulk_in_buffer_1[1]);

    return MIN(cnt, read_cnt);
    
 //return 0;
}

static struct file_operations fops = {

    .read    =  device_read,
    .write   =  device_write,
    .open    =  device_open,   
    .release =  device_release,
    .owner   =  THIS_MODULE,

};

static struct usb_class_driver NI_class_driver =
{
    .name        = "National_Instruments_Corp.",
    .fops        = &fops,
    .minor_base  = 200,

};


static int NI_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
    printk(KERN_INFO " National Instrument drive mit VendorID (%04X) und ProductID (%04X) ist Verbunden.\n", id->idVendor, id->idProduct);
    device = interface_to_usbdev(interface);
    int err;
    if((err = usb_register_dev(interface , &NI_class_driver)))
     { 
        printk(KERN_INFO " Die Registrierung von Device ist mit folgenden Fehler fehlgeschlagen: %d\n", err);
        usb_set_intfdata(interface, NULL);
        
     }
     else
     {
          set_bulk_address(interface);
          printk(KERN_INFO "USB_Device erfolgreich regestriert.\n");
          printk(KERN_INFO "Device Eigenschaften sind (%ld,%X,%X,%X,%X).\n",bulk_in_size, bulk_in_add_1, bulk_out_add_1, bulk_in_add_2, bulk_out_add_2);              
     }
    return err;
}

static void NI_disconnect(struct usb_interface *interface)
{
     printk(KERN_INFO "National Instrument Driver wird entfernt.\n");
     
     struct usb_driver *dev1;    
     
     int minor = interface->minor;    

     //lock_kernel();    
     
     dev1 = usb_get_intfdata(interface);   
     
     usb_set_intfdata(interface, NULL);     
     
     usb_deregister_dev(interface, &NI_class_driver);

     //unlock_kernel( );
     printk(KERN_INFO "national instrument driver wurde entfernt.\n");

}

static struct usb_device_id NI_table[] =
{
    { USB_DEVICE(0x3923, 0x717a) },
    // für weitere Anschlüsse
    {}
};
MODULE_DEVICE_TABLE (usb, NI_table);

static struct usb_driver NI_driver =
{
    .name        = "National_Instruments_Corp.",
    .probe       = NI_probe,
    .disconnect  = NI_disconnect,
    .id_table    = NI_table,

};

static int __init NI_init(void)
{
    int result = usb_register(&NI_driver);
    
    if (result)
        printk(KERN_ERR "Die Regestrierung von Kernal ist mit der folgenden Nummer ist fehlgeschlagen:  %d", result);

    return result;

}

static void __exit NI_exit(void)
{
    usb_deregister(&NI_driver);
}

module_init(NI_init);
module_exit(NI_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jihad Abu Dabat jihad.dabat@uni-jena.de");
MODULE_DESCRIPTION("National_Instruments Corp.");
