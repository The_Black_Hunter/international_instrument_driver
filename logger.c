#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions

static char * device  = "cat /dev/keyboard_logger";
static char * logfile = "/home/jo/Schreibtisch/keyboard_logger.log";

int BUFFER_SIZE = 100;
char data_in[1];
int main( )
{
FILE *logfile_;
   logfile_ = fopen(logfile, "a");

printf("the file is initwd %d",logfile_);

int fd_serialport;
struct termios options;
 
fd_serialport = open("/dev/keyboard_logger", O_RDWR | O_NOCTTY | O_NDELAY);
 
if(fd_serialport == -1){
    perror("Unable to open /dev/keyboard_logger");
}
 
tcgetattr(fd_serialport, &options);
cfsetispeed(&options, B38400);
cfsetospeed(&options, B38400);
options.c_cflag |= (CLOCAL | CREAD);    
options.c_cflag |= PARENB;
options.c_cflag |= PARODD;
options.c_cflag &= ~CSTOPB;
options.c_cflag &= ~CSIZE;
options.c_cflag |= CS8;
options.c_iflag |= (INPCK | ISTRIP);
tcsetattr(fd_serialport, TCSANOW, &options);
     
fcntl(fd_serialport, F_SETFL, FNDELAY);


while(read(fd_serialport, &data_in[0], sizeof(char))){
         
    if(data_in[0] != '\0'){
       printf("%c\n",data_in[0]);

       fputs(data_in, logfile_);

    }
    data_in[0] = '\0'; 
}

   fclose(logfile_);
}